/* Creación BD */
DROP DATABASE IF EXISTS rentroom;

CREATE DATABASE rentroom;
USE rentroom;

/* Eliminamos tablas */
DROP TABLE IF EXISTS imagenes_anuncios;
DROP TABLE IF EXISTS usuarios_chats;
DROP TABLE IF EXISTS idiomas_usuarios;
DROP TABLE IF EXISTS idiomas;
DROP TABLE IF EXISTS servicios_anuncios;
DROP TABLE IF EXISTS servicios;
DROP TABLE IF EXISTS anuncios;
DROP TABLE IF EXISTS usuarios;

/* Creamos tablas */
CREATE TABLE usuarios (id integer PRIMARY KEY AUTO_INCREMENT, nombre varchar(50), email varchar(50), password varchar(255), edad integer, imagen varchar(255), genero varchar(50));

CREATE TABLE idiomas (id integer PRIMARY KEY AUTO_INCREMENT, nombre varchar(50));

CREATE TABLE idiomas_usuarios (id integer PRIMARY KEY AUTO_INCREMENT, id_usuario integer, id_idioma integer, FOREIGN KEY (id_usuario) REFERENCES usuarios(id), FOREIGN KEY (id_idioma) REFERENCES idiomas(id));

CREATE TABLE anuncios (id integer PRIMARY KEY AUTO_INCREMENT, precio integer, direccion varchar(100), provincia varchar(50), ciudad varchar(50), imagen varchar(255), descripcion LONGTEXT , fecha_publicacion TIMESTAMP, lavabo boolean, tamano_hab integer, tamano_casa integer, num_hab integer, tamano_cama varchar(20), num_camas integer, ventana varchar(20), activo boolean, id_usuario integer, FOREIGN KEY (id_usuario) REFERENCES usuarios (id));

CREATE TABLE servicios (id integer PRIMARY KEY AUTO_INCREMENT, nombre varchar(100));

CREATE TABLE servicios_anuncios (id integer PRIMARY KEY AUTO_INCREMENT, id_anuncio integer, id_servicio integer, FOREIGN KEY (id_anuncio) REFERENCES anuncios(id), FOREIGN KEY (id_servicio) REFERENCES servicios(id));

CREATE TABLE usuarios_chats (id integer PRIMARY KEY AUTO_INCREMENT, id_user integer, id_anuncio integer, fecha TIMESTAMP, FOREIGN KEY (id_user) REFERENCES usuarios(id), FOREIGN KEY (id_anuncio) REFERENCES anuncios(id));

CREATE TABLE imagenes_anuncios (id integer PRIMARY KEY AUTO_INCREMENT, imagen varchar(255), id_anuncio integer, FOREIGN KEY(id_anuncio) REFERENCES anuncios(id));
