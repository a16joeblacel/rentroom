# Para ejecutar el script: "bash git_config.sh"

# Si ya tienes git instalado comenta esta línea
sudo apt-get --assume-yes install git

git config --global user.name Nombre Apellido
git config --global user.email email@gmail.com

echo "Git configurado correctamente"
