var routes = require("./app/routes/route.js");

const sequelize = require('./app/model/config_db.js');
const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');

var app = express()


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(cors());

var port = process.env.PORT || 3000;
app.set('port', port);

app.use('/', routes);

// Comprovar conexión BD y arrancar servidor node
sequelize
    .authenticate()
    .then(() => {
        console.log('Conexión a la BD establecida correctamente.');

        app.listen(app.get('port'), function () {
            console.log('RentRoom corriendo perfectamente en el puerto: ' + port);
        });
    })
    .catch(err => {
        console.error('No se ha podido establecer la conexión con la base de datos: ', err);
    });




