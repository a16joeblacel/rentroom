var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./config_token.js');

exports.createToken = function (id) {

    var payload = {

        sub: id,
        iat: moment().unix(),
        exp: moment().add(7, "days").unix(),
    };

    return jwt.encode(payload, config.TOKEN_SECRET);
};