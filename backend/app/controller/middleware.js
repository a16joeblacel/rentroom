var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./config_token.js');

exports.checkAuth = function (req, res, next) {
    if (!req.headers.authorization) {
        return res
            .status(403)
            .send({ message: "Tu petición no tiene cabecera de autorización" });
    }

    var token = req.headers.authorization.split(" ")[1];
    var payload = jwt.decode(token, config.TOKEN_SECRET);

    if (payload.exp <= moment().unix()) {
        return res
            .status(401)
            .send({ message: "El token ha expirado" });
    }

    // Pasamos el id del usuario en la request
    req.user = payload.sub;

    next();
}

exports.checkOptionalAuth = function (req, res, next) {

    if (!req.headers.authorization) {
        next()

    } else {

        var token = req.headers.authorization.split(" ")[1];
        var payload = jwt.decode(token, config.TOKEN_SECRET);

        if (payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({ message: "El token ha expirado" });
        }

        // Pasamos el id del usuario en la request
        req.user = payload.sub;

        next();
    }
}