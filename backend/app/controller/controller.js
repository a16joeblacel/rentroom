const entities = require('../model/model.js');
const service = require('./services.js');
const crypto = require('./encryption.js')
const Sequelize = require('sequelize');
const Op = Sequelize.Op

var path = require('path');
var formidable = require('formidable')
var fs = require('fs');

// Obtenemos todos los anuncios de la tabla anuncios y se los pasamos en la respuesta
exports.getAllAnuncios = function (req, res) {

    entities.Anuncios.findAll({
        where: {
            activo: true
        }
    })
        .then(result => {

            res.json(result);

        })
        .catch(err => {
            console.log(err)
        })

}

// Recibimos los parámetros del anuncio y los guardamos en un nuevo registro en la tabla anuncios
exports.createAnuncio = function (req, res) {

    var anuncioParams;
    var imageName;
    var fileExt;
    var serviciosParams;

    var form = new formidable.IncomingForm({
        keepExtensions: true,
        maxFieldsSize: 10 * 1024 * 1024,
    });

    form
        .on('error', function (err) {
            throw err;
        })

        .on('field', function (field, value) {

            if (field == "anuncio") {
                anuncioParams = value;

            } else if (field == "servicios") {
                serviciosParams = value

            }
        })

        .on('fileBegin', function (name, file) {

            fileExt = file.name.split('.').pop();
            let date = new Date();

            imageName = `anuncio_${date.getTime()}.${fileExt}`

            file.path = path.join("/imagenes/anuncios", imageName);

        })

        .on('end', function () {

            var data = anuncioParams.split(',');


            if (data[4] == "individual") {
                data[4] = false
            } else {
                data[4] = true
            }

            entities.Anuncios.create({
                precio: data[0],
                provincia: data[1],
                ciudad: data[2],
                direccion: data[3],
                lavabo: data[4],
                tamano_casa: data[5],
                tamano_hab: data[6],
                num_hab: data[7],
                tamano_cama: data[8],
                num_camas: data[9],
                ventana: data[10],
                descripcion: data[11],
                id_usuario: req.user,
                imagen: imageName,
                activo: true

            }).then(result => {

                result.update(
                    { imagen: `anuncio_${result.dataValues.id}.${fileExt}` },
                    { where: { id: result.dataValues.id } }

                ).then(result3 => {

                    let servicios = serviciosParams.split(',');

                    let counter = servicios.length
                    servicios.forEach(servicio => {

                        entities.Servicios.findOne({
                            where: { nombre: servicio.toLowerCase() }

                        }).then(result_servicio => {

                            entities.Servicios_Anuncios.create({
                                id_anuncio: result.dataValues.id,
                                id_servicio: result_servicio.dataValues.id

                            }).then(result_createservice => {

                                counter -= 1

                                if (counter == 0) {

                                    fs.rename(`/imagenes/anuncios/${imageName}`, `/imagenes/anuncios/anuncio_${result.dataValues.id}.${fileExt}`, function (err) {
                                        if (err) console.log('ERROR: ' + err);
                                    });

                                    res.json({ "created": true, "anuncio": result.dataValues.id });
                                }

                            }).catch(error_createservice => {
                                console.log(error_createservice)
                            })

                        }).catch(error_servicio => {

                        })

                    })

                })

            }).catch(err => {
                console.log(err)
            });


        });

    form.parse(req);

}

// Guarda cada una de las imagenes que recibe en la base de datos y en el servidor 
exports.uploadImageGaleria = function (req, res) {

    var imageName
    var fileExt;
    var formParams;

    var form = new formidable.IncomingForm({
        keepExtensions: true,
        maxFieldsSize: 10 * 1024 * 1024,
    });

    form
        .on('error', function (err) {
            throw err;
        })

        .on('field', function (field, value) {
            formParams = value;
        })

        .on('fileBegin', function (name, file) {

            fileExt = file.name.split('.').pop();
            let date = new Date();

            imageName = `anuncio_${date.getTime()}.${fileExt}`

            file.path = path.join("/imagenes/anuncios/galeria", imageName);

        })

        .on('end', function () {

            var data = formParams.split(',');

            entities.Imagenes_Anuncios.create({
                imagen: 'asdasd',
                id_anuncio: data[0]

            }).then(result => {

                result.update(
                    { imagen: `anuncio_galeria_${result.dataValues.id}.${fileExt}` },
                    { where: { id: result.dataValues.id } }

                ).then(result3 => {

                    fs.rename(`/imagenes/anuncios/galeria/${imageName}`, `/imagenes/anuncios/galeria/anuncio_galeria_${result.dataValues.id}.${fileExt}`, function (err) {
                        if (err) console.log('ERROR: ' + err);
                    });

                    res.json({ "galeria": "ok" })

                })

            }).catch(error => {

                console.log(error)

            })

        });

    form.parse(req);

}

// Dado un id devuelve toda la información del anuncio y 3 anuncios aleatorios
exports.getAnuncio = function (req, res) {

    let id = req.query.id
    let id_user = req.user

    entities.Anuncios.findOne({
        where: {
            id: id
        },
        include: [{
            model: entities.Usuarios,
            include: [{
                model: entities.Idiomas
            }],
        }, {
            model: entities.Servicios
        }, {
            model: entities.Imagenes_Anuncios
        }]
    }).then(result => {

        if (result != null) {

            entities.Anuncios.findAll({
                where: {
                    ciudad: result.dataValues.ciudad,
                    id: { [Op.ne]: id }
                },
                order: Sequelize.literal('rand()'),
                limit: 3

            }).then(result_rand => {

                let anunciosRandom = []

                if (result_rand.length != 0) {

                    let counter = result_rand.length;

                    result_rand.forEach(anuncio => {

                        anunciosRandom.push(anuncio.dataValues)

                        counter -= 1;

                        if (counter === 0) {

                            if (result_rand.length != 3) {

                                let missing = 3 - result_rand.length
                                let notAllowed = [parseInt(id)]

                                for (let x = 0; x < result_rand.length; x++) {

                                    notAllowed.push(result_rand[x].dataValues.id)

                                }

                                entities.Anuncios.findAll({
                                    where: {
                                        id: { [Op.notIn]: notAllowed }
                                    },
                                    order: Sequelize.literal('rand()'),
                                    limit: missing

                                }).then(result_missing => {

                                    for (let j = 0; j < result_missing.length; j++) {

                                        anunciosRandom.push(result_missing[j].dataValues)

                                    }

                                    if (id_user != null || id_user != undefined) {

                                        if (result.dataValues.id_usuario == id_user) {

                                            res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": true })

                                        } else {

                                            res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": false })

                                        }

                                    } else {

                                        res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": false })

                                    }

                                }).catch(error_missing => {

                                    console.log(error_missing)

                                })

                            } else {

                                if (id_user != null || id_user != undefined) {

                                    if (result.dataValues.id_usuario == id_user) {

                                        res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": true })

                                    } else {

                                        res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": false })

                                    }

                                } else {

                                    res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": false })

                                }

                            }

                        }

                    })

                } else {

                    entities.Anuncios.findAll({
                        where: {
                            id: { [Op.ne]: id }
                        },
                        order: Sequelize.literal('rand()'),
                        limit: 3

                    }).then(result_none => {

                        for (let j = 0; j < result_none.length; j++) {

                            anunciosRandom.push(result_none[j].dataValues)

                        }

                        if (id_user != null || id_user != undefined) {

                            if (result.dataValues.id_usuario == id_user) {

                                res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": true })

                            } else {

                                res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": false })

                            }

                        } else {

                            res.json({ "status": true, "anuncio": result, "random_anuncios": anunciosRandom, "own": false })

                        }

                    }).catch(error_none => {
                        console.log(error_none)
                    })


                }

            }).catch(error => {
                console.log(error)

                res.json({ "status": false })
            })

        } else {

            res.json({ "status": false })

        }

    }).catch(error_gen => {

        console.log(error_gen)

        res.json({ "status": false })

    })
}

// Recibe los parámetros del anuncio, busca si existe y si lo encuentra, lo actualiza
exports.updateAnuncio = function (req, res) {

    let id_anuncio = req.body.id
    let dataUpdate = req.body.update

    if (dataUpdate.lavabo == "individual") {
        dataUpdate.lavabo = false
    } else {
        dataUpdate.lavabo = true
    }


    entities.Anuncios.findOne({
        where: {
            id: id_anuncio
        }
    }).then(result_anuncios => {

        if (result_anuncios != null) {

            result_anuncios.update({
                precio: dataUpdate.precio,
                direccion: dataUpdate.direccion,
                provincia: dataUpdate.provincia,
                ciudad: dataUpdate.ciudad,
                lavabo: dataUpdate.lavabo,
                descripcion: dataUpdate.descripcion,
                tamano_hab: dataUpdate.tam_hab,
                tamano_casa: dataUpdate.tam_casa,
                num_hab: dataUpdate.num_hab,
                tamano_cama: dataUpdate.tam_cama,
                num_camas: dataUpdate.num_camas,
                ventana: dataUpdate.ventana

            }).then(result_update => {

                if (result_update) {
                    res.json({ "updated": true })
                } else {
                    res.json({ "updated": false })
                }

            }).catch(error_update => {

                console.log(error_update)

            })


        } else {

            // Anuncio no encontrado
            res.json({ "updated": false })

        }

    }).catch(error_anuncios => {

        console.log(error_anuncios)

    })

}

// Dado un id elimina el anuncio y todos los datos asociados a él
exports.deleteAnuncio = function (req, res) {

    let id = req.body.id

    entities.Usuarios_Chat.destroy({
        where: {
            id_anuncio: id
        }

    }).then(result => {

        entities.Servicios_Anuncios.destroy({
            where: {
                id_anuncio: id
            }
        }).then(result => {

            entities.Imagenes_Anuncios.destroy({
                where: {
                    id_anuncio: id
                }
            }).then(result => {

                entities.Anuncios.destroy({
                    where: {
                        id: id
                    }
                }).then(result => {
                    res.json(result)
                }).catch(error => {
                    console.log(error)
                })

            }).catch(error => {
                console.log(error)
            })

        }).catch(error => {
            console.log(error)
        })

    }).catch(error => {
        console.log(error)
    })


}

// Guarda los datos del usuario en la tabla usuarios y el password encriptado, devuelve un token
exports.register = function (req, res) {

    let email = req.body.email
    let password = req.body.password
    let nombre = req.body.nombre

    entities.Usuarios.create({
        email: email,
        nombre: nombre,
        password: crypto.encrypt(password)

    }).then(result => {

        let id = result.dataValues.id

        return res
            .status(200)
            .send({ token: service.createToken(id) });

    }).catch(error => {
        console.log(error)
    })


}

// Si el email y la contraseña con los de la BD coinciden devuelve el token
exports.login = function (req, res) {

    let email = req.body.email
    let password = req.body.password

    entities.Usuarios.findOne({
        where: {
            email: email,
        }
    }).then(result => {

        if (result == null) {

            return res
                .status(401)
                .send("El usuario no existe")

        } else {

            let id = result.dataValues.id

            if (crypto.encrypt(password) === result.dataValues.password) {

                return res
                    .status(200)
                    .send({ token: service.createToken(id), user_id: id, nombre: result.dataValues.nombre });

            } else {

                return res
                    .status(401)
                    .send("La contraseña es incorrecta")

            }
        }

    }).catch(error => {
        console.log(error)
    })

}


// Guarda en la BD cuando un usuario inicia un chat con otro
exports.userChat = function (req, res) {

    let id_anuncio = req.body.anuncio
    let id_user = req.user

    entities.Usuarios_Chat.findOne({
        where: {
            id_anuncio: id_anuncio,
            id_user: id_user
        }
    }).then(result => {

        if (result == null) {

            entities.Usuarios_Chat.create({

                id_anuncio: id_anuncio,
                id_user: id_user

            }).then(result2 => {

                console.log(result2)
                res.json({ 'chat': 'nuevo', 'id_usuario': req.user })

            }).catch(error => {

                console.log(error)

            })

        } else {

            res.json({ 'chat': 'iniciado', 'id_usuario': req.user })

        }

    }).catch(error => {

        console.log(error)

    })

}

// Recibe los datos del perfil y si el usuario existe, actualiza sus datos
exports.updateProfile = function (req, res) {

    entities.Usuarios.findOne({
        where: {
            id: req.user
        }
    }).then(result => {

        if (result) {

            var formParams;
            var imageName;

            var form = new formidable.IncomingForm({
                keepExtensions: true,
            });

            form
                .on('error', function (err) {
                    throw err;
                })

                .on('field', function (field, value) {

                    formParams = value;

                })

                .on('fileBegin', function (name, file) {

                    let fileExt = file.name.split('.').pop();

                    imageName = `avatar_${req.user}.${fileExt}`

                    file.path = path.join("/imagenes/usuarios/", imageName);

                })

                .on('end', function () {

                    var data = formParams.split(',');

                    result.update({
                        nombre: data[0],
                        edad: data[1],
                        genero: data[2],
                        imagen: imageName

                    }).then(result1 => {

                        entities.Idiomas_Usuarios.destroy({
                            where: { id_usuario: req.user }

                        }).then(result_delete => {

                            for (let x = 3; x < data.length; x++) {

                                entities.Idiomas.findOne({
                                    where: { nombre: data[x] }
                                }).then(result_idioma => {

                                    entities.Idiomas_Usuarios.create({
                                        id_usuario: req.user,
                                        id_idioma: result_idioma.dataValues.id

                                    }).then(result_create => {

                                        if (x == data.length) {
                                            res.json("updated")
                                        }

                                    }).catch(error_create => {

                                        console.log(error_create)

                                    })

                                }).catch(error_idioma => {

                                    console.log(error_idioma)

                                })

                            }

                        }).catch(error_delete => {

                            console.log(error_delete)

                        })

                    })


                });

            form.parse(req);


        }

    }).catch(error => {

        console.log(error)

    })

}

// Devuelve un array con la información del anuncio y del usuario con el que hemos iniciado un chat
exports.ConversacionesPropias = function (req, res) {

    let id_user = req.user
    let conversacionesPropias = []

    entities.Usuarios_Chat.findAll({
        where: { id_user: id_user }

    }).then(result => {

        if (result.length != 0) {

            let counter = result.length
            for (let x = 0; x < result.length; x++) {

                entities.Anuncios.findOne({
                    where: { id: result[x].dataValues.id_anuncio }

                }).then(result_anuncios => {

                    entities.Usuarios.findOne({
                        where: { id: result_anuncios.dataValues.id_usuario }

                    }).then(result_usuarios => {

                        conversacionesPropias.push({ usuario: result_usuarios.dataValues, anuncio: result_anuncios.dataValues })

                        counter -= 1

                        if (counter == 0) {

                            res.json({ conversaciones: conversacionesPropias, id_usuario: req.user })

                        }

                    })

                })

            }

        } else {

            res.json({ conversaciones: conversacionesPropias, id_usuario: req.user })

        }


    })

}

// Devuelve un array con la información del anuncio y del usuario que ha iniciado un chat con un anuncio nuestro
exports.ConversacionesMisAnuncios = function (req, res) {

    let id_user = req.user
    let conversacionesMisAnuncios = []

    entities.Anuncios.findAll({
        where: { id_usuario: id_user },
        include: [
            { model: entities.Usuarios, as: 'UserChats_Usuarios' },
        ]
    }).then(result => {

        let counter = result.length

        result.forEach(anuncio => {

            counter -= 1

            let direccion = anuncio.dataValues.direccion
            let precio = anuncio.dataValues.precio
            let id_anuncio = anuncio.dataValues.id

            for (x = 0; x < anuncio.dataValues.UserChats_Usuarios.length; x++) {

                conversacionesMisAnuncios.push({ usuario: anuncio.dataValues.UserChats_Usuarios[x].dataValues, direccion: direccion, precio: precio, id_anuncio: id_anuncio })

                if (counter == 0 && x == anuncio.dataValues.UserChats_Usuarios.length - 1) {

                    res.json({ conversaciones: conversacionesMisAnuncios, id_usuario: req.user })

                }

            }
        })
    })

}

// Obtiene la información del perfil de un usuario dado su id
exports.getUserProfile = function (req, res) {

    let id_user = req.user

    entities.Usuarios.findOne({
        attributes: ['id', 'nombre', 'edad', 'imagen', 'genero'],
        where: {
            id: id_user
        },
        include: [{
            model: entities.Idiomas,
            attributes: ['nombre']
        }]

    }).then(result => {

        res.json(result)

    }).catch(error => {

        console.log(error)

    })
}

// Obtiene los anuncios del usuario logueado
exports.ownAnuncios = function (req, res) {

    let id_user = req.user

    entities.Anuncios.findAll({
        where: {
            id_usuario: id_user
        }
    }).then(result => {
        res.json(result)
    }).catch(error => {
        console.log(error)
    })
}

// Devuelve un contador de la cantidad de anuncios en cada ciudad
exports.anunciosCiudades = function (req, res) {

    entities.Anuncios.findAll({
        where: {
            provincia: { [Op.in]: ['Barcelona', 'Madrid', 'Sevilla', 'Valencia', 'Tarragona', 'Islas Baleares', 'Zaragoza'] }
        }
    }).then(result => {

        let count_bcn = 0
        let count_madrid = 0
        let count_sevilla = 0
        let count_valencia = 0
        let count_tarragona = 0
        let count_islasbaleares = 0
        let count_zaragoza = 0

        let counter = result.length

        result.forEach(anuncio => {

            switch (anuncio.dataValues.provincia) {

                case "Barcelona": {

                    count_bcn++
                    break;

                }
                case "Madrid": {

                    count_madrid++
                    break;

                }
                case "Sevilla": {

                    count_sevilla++
                    break;

                }
                case "Valencia": {

                    count_valencia++
                    break;

                }
                case "Tarragona": {

                    count_tarragona++
                    break;

                }
                case "Islas Baleares": {

                    count_islasbaleares++
                    break;

                }
                case "Zaragoza": {

                    count_zaragoza++
                    break;

                }

            }

            counter -= 1
            if (counter == 0) {

                res.json({ barcelona: count_bcn, madrid: count_madrid, sevilla: count_sevilla, valencia: count_valencia, tarragona: count_tarragona, baleares: count_islasbaleares, zaragoza: count_zaragoza })

            }

        })

    }).catch(error => {

        console.log(error)

    })

}

exports.getCiudad = (req, res) => {

    let provincia = req.query.provincia

    entities.Anuncios.findAll({
        where: {
            provincia: provincia
        }
    }).then(result => {

        let anuncios = []

        for (let x = 0; x < result.length; x++) {

            anuncios.push(result[x].dataValues)

        }

        res.json(anuncios)

    }).catch(error => {

        console.log(error)

    })
}

// Cambia el estado del anuncio a activo / inactivo
exports.anuncioAlquilado = (req, res) => {

    let id = req.body.id

    entities.Anuncios.findOne({
        where: {
            id: id
        }
    }).then(result => {

        if (result.length != 0) {

            let estado = true

            if (result.dataValues.activo) {
                estado = false
            } else {
                estado = true
            }

            result.update(
                { activo: estado },
                { where: { id: result.dataValues.id } }

            ).then(result_update => {

                res.json({ 'updated': true })

            }).catch(error_update => {
                console.log(error_update)
            })

        } else {

            res.json({ 'updated': false })

        }

    }).catch(error => {
        console.log(error)
    })

}
