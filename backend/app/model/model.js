const Sequelize = require('sequelize');
const sequelize = require('./config_db.js');

//exports.entities = function () {

const Usuarios = sequelize.define('usuarios', {
  nombre: {
    type: Sequelize.STRING,
  },
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  edad: {
    type: Sequelize.INTEGER
  },
  imagen: {
    type: Sequelize.STRING
  },
  genero: {
    type: Sequelize.STRING
  }
})

const Idiomas = sequelize.define('idiomas', {
  nombre: {
    type: Sequelize.STRING,
  },
})

const Idiomas_Usuarios = sequelize.define('idiomas_usuarios', {
  id_usuario: {
    type: Sequelize.INTEGER,
  },
  id_idioma: {
    type: Sequelize.INTEGER,
  },
})

const Anuncios = sequelize.define('anuncios', {
  precio: {
    type: Sequelize.INTEGER
  },
  direccion: {
    type: Sequelize.STRING
  },
  provincia: {
    type: Sequelize.STRING
  },
  ciudad: {
    type: Sequelize.STRING
  },
  imagen: {
    type: Sequelize.STRING
  },
  descripcion: {
    type: Sequelize.STRING
  },
  fecha_publicacion: {
    type: Sequelize.DATE
  },
  lavabo: {
    type: Sequelize.BOOLEAN
  },
  tamano_hab: {
    type: Sequelize.INTEGER
  },
  tamano_casa: {
    type: Sequelize.INTEGER
  },
  num_hab: {
    type: Sequelize.INTEGER
  },
  tamano_cama: {
    type: Sequelize.STRING
  },
  num_camas: {
    type: Sequelize.INTEGER
  },
  ventana: {
    type: Sequelize.STRING
  },
  activo: {
    type: Sequelize.BOOLEAN
  }
})

const Servicios = sequelize.define('servicios', {
  nombre: {
    type: Sequelize.STRING
  }
})

const Servicios_Anuncios = sequelize.define('servicios_anuncios', {
  id_anuncio: {
    type: Sequelize.STRING
  },
  id_servicio: {
    type: Sequelize.STRING
  }
})

const Usuarios_Chat = sequelize.define('usuarios_chats', {
  id_user: {
    type: Sequelize.INTEGER
  },
  id_anuncio: {
    type: Sequelize.INTEGER
  },
  fecha: {
    type: Sequelize.DATE
  }

})

const Imagenes_Anuncios = sequelize.define('imagenes_anuncios', {
  imagen: {
    type: Sequelize.STRING
  },
  id_anuncio: {
    type: Sequelize.INTEGER
  }
})



// Relaciones entre tablas
Usuarios.hasMany(Anuncios, { foreignKey: 'id_usuario' })
Anuncios.belongsTo(Usuarios, { foreignKey: 'id_usuario' })

Idiomas.belongsToMany(Usuarios, { through: Idiomas_Usuarios, foreignKey: 'id_idioma' })
Usuarios.belongsToMany(Idiomas, { through: Idiomas_Usuarios, foreignKey: 'id_usuario' })

Servicios.belongsToMany(Anuncios, { through: Servicios_Anuncios, foreignKey: 'id_servicio' })
Anuncios.belongsToMany(Servicios, { through: Servicios_Anuncios, foreignKey: 'id_anuncio' })

Usuarios.belongsToMany(Anuncios, { through: Usuarios_Chat, as: 'UserChats_Anuncios', foreignKey: 'id_user' })
Anuncios.belongsToMany(Usuarios, { through: Usuarios_Chat, as: 'UserChats_Usuarios', foreignKey: 'id_anuncio' })

Anuncios.hasMany(Imagenes_Anuncios, { foreignKey: 'id_anuncio' })
//}

module.exports = { Usuarios: Usuarios, Anuncios: Anuncios, Idiomas: Idiomas, Servicios: Servicios, Usuarios_Chat: Usuarios_Chat, Servicios_Anuncios: Servicios_Anuncios, Imagenes_Anuncios: Imagenes_Anuncios, Idiomas_Usuarios: Idiomas_Usuarios };


