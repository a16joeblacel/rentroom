const Sequelize = require('sequelize');

const sequelize = new Sequelize('rentroom', 'root', 'ausias', {
  host: 'localhost',
  dialect: 'mysql', /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  define: {
    timestamps: false
  }
});

module.exports = sequelize;
