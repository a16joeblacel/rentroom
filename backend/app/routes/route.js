const controller = require('../controller/controller.js');
const middleware = require('../controller/middleware.js');

var express = require('express')
var router = express.Router();

// Si requiere autenticación --> middleware.checkAuth
router.route('/anuncios')
    .get(controller.getAllAnuncios)

router.route('/anuncio')
    .get(middleware.checkOptionalAuth, controller.getAnuncio)
    .post(middleware.checkAuth, controller.createAnuncio)
    .put(middleware.checkAuth, controller.updateAnuncio)
    .delete(middleware.checkAuth, controller.deleteAnuncio)

router.route('/register')
    .post(controller.register)

router.route('/login')
    .post(controller.login)

router.route('/chat')
    .post(middleware.checkAuth, controller.userChat)

router.route('/myanuncios')
    .get(middleware.checkAuth, controller.ownAnuncios)

router.route('/conversacionesmisanuncios')
    .get(middleware.checkAuth, controller.ConversacionesMisAnuncios)

router.route('/conversacionespropias')
    .get(middleware.checkAuth, controller.ConversacionesPropias)

router.route('/user')
    .get(middleware.checkAuth, controller.getUserProfile)
    .put(middleware.checkAuth, controller.updateProfile)

router.route('/ciudades')
    .get(controller.anunciosCiudades)

router.route('/anunciosciudad')
    .get(controller.getCiudad)

router.route('/anuncioalquilado')
    .put(middleware.checkAuth, controller.anuncioAlquilado)

router.route('/imagengaleria')
    .post(middleware.checkAuth, controller.uploadImageGaleria)

module.exports = router;



