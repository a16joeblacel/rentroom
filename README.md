# RentRoom

### Descripción

* Página web que permite encontrar habitaciones en alquiler y permite a propietarios publicar anuncios de sus habitaciones.

### Tecnologias Utilizadas

* FrontEnd: VueJs + JQuery
* BackEnd: Nodejs + Mysql


### Desarrolladores

* Fabiana Arzamendia 
* Joel Blanco
* Mabe Chávez 

### Moqups

[Moqups](https://app.moqups.com/4X8rQj7eeA/view)

### Versión

1.0

