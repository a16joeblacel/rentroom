
var login = new Vue({
    el: '.site-navbar',
    data: {
        user: {
            email: "",
            password: "",
            nombre: ""
        },
        login_form: false,
        logged: true,

    },
    mounted() {

        if (localStorage.getItem("token") != null) {
            this.logged = false
            this.user.nombre = localStorage.getItem('nombre')

        }

    },
    methods: {

        // Al hacer click en Login mostramos la caja del login
        show_boxlogin: function () {

            if (this.login_form) {
                this.login_form = false
            } else {
                this.login_form = true
            }
        },

        // Cuando se loguean mandamos el email y la password y comprovamos que coincidan con la BD
        login: function () {

            axios.post(`http://${ip}:${puerto}/login`, {

                email: login.user.email,
                password: login.user.password

            }).then(function (res) {


                if (res.status == 200) {
                    localStorage.setItem("token", res.data.token);
                    localStorage.setItem("nombre", res.data.nombre);

                    login.nombre = res.data.nombre


                    location.reload();

                } else {
                    alert("Error extraño con el token")
                }

            }).catch(function (error) {

                alert("Email o contraseña incorrecta")

            })

        },

        logout: function () {
            localStorage.removeItem("token")
            localStorage.removeItem("user_id")
        },

    },

})
