
var single = new Vue({
    el: '.single-ciudad',
    data: {
        anuncios: [],
        ciudad: ''
    },
    mounted() {
        this.nombreCiudad()
        this.getCiudades()
    },
    methods: {

        // Obtenemos el nombre de la ciudad a partir de la Url
        nombreCiudad: function () {

            let uri = window.location.search.substring(1);
            let params = new URLSearchParams(uri);

            let ciudad = params.get("ciudad")
            this.ciudad = ciudad

        },

        // Obtenemos todos los anuncios de la ciudad escogida y las mostramos
        getCiudades: function () {

            axios.get(`http://${ip}:${puerto}/anunciosciudad/?provincia=${this.ciudad}`).then(function (res) {

                console.log(res)

                let arrayAnuncios = [];

                res.data.forEach(anuncio => {
                    anuncio.imagen = `http://${ip_nginx}:80/anuncios/${anuncio.imagen}`
                    arrayAnuncios.push(anuncio);
                })

                single.anuncios = arrayAnuncios

            }).catch(error => {

                $.growl.error({ location: 'tr', message: "No se han podido obtener las habitaciones para esta ciudad" });

            })

        }

    }

})