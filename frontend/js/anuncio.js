var showAnuncio = new Vue({
    el: '#inicial_anuncio',
    data: {
        anuncio: "",
        usuario: "",
        chat_option: false,
        own_id: '',
        random_anuncios: [],
        img_num: 0
    },
    mounted() {
        this.getDataAnuncio()
        this.allowChat()
        this.setImagePopup()
    },
    methods: {

        // Mostramos el boton de chat si está logueado
        allowChat: function () {

            if (localStorage.getItem("token") != null) {

                this.chat_option = true;
            }

        },

        // Obtenemos los datos del anuncio para mostrarlos
        getDataAnuncio: function () {

            let uri = window.location.search.substring(1);
            let params = new URLSearchParams(uri);

            let id = params.get("id")

            if (localStorage.getItem("token") != null) {

                var config = {
                    headers: { 'Authorization': "bearer " + localStorage.getItem("token") }
                };

                axios.get(`http://${ip}:${puerto}/anuncio?id=${id}`, config).then(function (res) {

                    if (res.data.status) {

                        showAnuncio.resultAnuncio(res)

                    } else {

                        window.location = "index.html"

                    }

                }).catch(error => {

                    $.growl.error({ location: 'tr', message: "No se ha podido obtener la información de la habitación" });

                })

            } else {

                axios.get(`http://${ip}:${puerto}/anuncio?id=${id}`).then(function (res) {

                    showAnuncio.resultAnuncio(res)

                }).catch(error => {

                    $.growl.error({ location: 'tr', message: "No se ha podido obtener la información de la habitación" });

                })

            }
        },

        // Recibimos el resultado de la petición al servidor y adaptamos imagenes y algunos campos
        resultAnuncio: function (res) {

            console.log(res)

            res.data.anuncio.imagen = `http://${ip_nginx}:80/anuncios/${res.data.anuncio.imagen}`
            res.data.anuncio.usuario.imagen = `http://${ip_nginx}:80/usuarios/${res.data.anuncio.usuario.imagen}`

            for (t = 0; t < res.data.anuncio.imagenes_anuncios.length; t++) {
                res.data.anuncio.imagenes_anuncios[t].imagen = `http://${ip_nginx}:80/anuncios/galeria/${res.data.anuncio.imagenes_anuncios[t].imagen}`
            }

            showAnuncio.anuncio = res.data.anuncio
            showAnuncio.usuario = res.data.anuncio.usuario

            console.log(showAnuncio.anuncio)

            console.log(showAnuncio.usuario)


            if (showAnuncio.anuncio.lavabo == false) {

                showAnuncio.anuncio.lavabo = "Individual";

            } else if (showAnuncio.anuncio.lavabo == true) {

                showAnuncio.anuncio.lavabo = "Compartido";
            }

            if (res.data.own) {
                showAnuncio.chat_option = false
            }

            $(".fondo_anuncio").css("background-image", `url(${showAnuncio.anuncio.imagen})`)
            setInterval(showAnuncio.changeBackground, 5000);

            res.data.random_anuncios.forEach(anuncio => {

                anuncio.imagen = `http://${ip_nginx}:80/anuncios/${anuncio.imagen}`
                showAnuncio.random_anuncios.push(anuncio)

            })

        },

        // Las imágenes del inicio van rotando entre ellas como si fuera un carrusel
        changeBackground: function () {

            let imagen = this.anuncio.imagenes_anuncios[this.img_num].imagen

            if (this.img_num == this.anuncio.imagenes_anuncios.length - 1) {
                this.img_num = 0;
            } else {
                this.img_num++
            }

            var image = $('.fondo_anuncio');
            image.fadeOut(200, function () {
                image.css("background-image", `url(${imagen}`);
                image.fadeIn(300);
            });

        },

        // Cuando le dan al bóton de chat guardamos la información en la BD y redirigimos a la página del chat
        startChat: function () {

            if (localStorage.getItem("token") != null) {

                var config = {
                    headers: { 'Authorization': "bearer " + localStorage.getItem("token") }
                };

                axios.post(`http://${ip}:${puerto}/chat`,
                    {
                        anuncio: showAnuncio.anuncio.id
                    },
                    config

                ).then(function (res) {

                    console.log(res)

                    let primer_chat = "";

                    if (res.data.chat == "iniciado") {
                        primer_chat = false
                    } else if (res.data.chat == "nuevo") {
                        primer_chat = true;
                    }

                    let user = showAnuncio.anuncio.usuario
                    showAnuncio.own_id = res.data.id_usuario

                    localStorage.setItem("user_chat", JSON.stringify({ nombre: user.nombre, id: user.id, own_id: showAnuncio.own_id, firsttime: primer_chat, id_firebase: showAnuncio.own_id, id_anuncio: showAnuncio.anuncio.id }));

                    window.location = "chat.html"

                }).catch(error => {

                    $.growl.error({ location: 'tr', message: "No se ha podido iniciar el chat" });

                })

            } else {

                alert("No estás logueado")

            }

        },

        // Para poder visualizar las imágenes de la galeria (Plantilla)
        setImagePopup: function () {

            jQuery(document).ready(function ($) {

                "use strict";

                var siteCarousel = function () {
                    if ($('.nonloop-block-13').length > 0) {
                        $('.nonloop-block-13').owlCarousel({
                            center: false,
                            items: 1,
                            loop: true,
                            stagePadding: 0,
                            autoplay: true,
                            margin: 20,
                            nav: false,
                            dots: true,
                            navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
                            responsive: {
                                600: {
                                    margin: 20,
                                    stagePadding: 0,
                                    items: 1
                                },
                                1000: {
                                    margin: 20,
                                    stagePadding: 0,
                                    items: 2
                                },
                                1200: {
                                    margin: 20,
                                    stagePadding: 0,
                                    items: 3
                                }
                            }
                        });
                    }

                    if ($('.slide-one-item').length > 0) {
                        $('.slide-one-item').owlCarousel({
                            center: false,
                            items: 1,
                            loop: true,
                            stagePadding: 0,
                            margin: 0,
                            autoplay: true,
                            pauseOnHover: false,
                            nav: true,
                            animateOut: 'fadeOut',
                            animateIn: 'fadeIn',
                            navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">']
                        });
                    }


                    if ($('.nonloop-block-4').length > 0) {
                        $('.nonloop-block-4').owlCarousel({
                            center: true,
                            items: 1,
                            loop: false,
                            margin: 10,
                            nav: true,
                            navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
                            responsive: {
                                600: {
                                    items: 1
                                }
                            }
                        });
                    }

                };
                siteCarousel();

                var siteMagnificPopup = function () {
                    $('.image-popup').magnificPopup({
                        type: 'image',
                        closeOnContentClick: true,
                        closeBtnInside: false,
                        fixedContentPos: true,
                        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                        gallery: {
                            enabled: true,
                            navigateByImgClick: true,
                            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image: {
                            verticalFit: true
                        },
                        zoom: {
                            enabled: true,
                            duration: 300 // don't foget to change the duration also in CSS
                        }
                    });

                    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
                        disableOn: 700,
                        type: 'iframe',
                        mainClass: 'mfp-fade',
                        removalDelay: 160,
                        preloader: false,

                        fixedContentPos: false
                    });
                };
                siteMagnificPopup();

            })


        }

    },

})