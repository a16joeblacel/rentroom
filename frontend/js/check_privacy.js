var count = new Vue({
    el: '#privacy-box',
    data: {

    },

    mounted() {

        // Comprovamos que haya leído la declaración de privacidad, mostramos la barra si no lo ha hechos
        if (localStorage.getItem("privacy_box") == null) {

            if (!localStorage.getItem("privacy_box")) {

                $(".cookieinfo").show()

                $(".but-cookies").click(() => {

                    localStorage.setItem("privacy_box", false)

                    $(".cookieinfo").hide()

                })
            }

        } else {

            $(".cookieinfo").hide()

        }

    },

})