// Configuración de la plantilla

AOS.init({
	duration: 800,
	easing: 'slide',
	once: true
});

jQuery(document).ready(function ($) {

	"use strict";

	// Loading page
	var loaderPage = function () {
		$(".site-loader").fadeOut("slow");
	};
	loaderPage();

});