
var app = new Vue({
    el: '#misChats',
    data: {
        chatsPropios: [],
        chatsConmigo: [],
        own_id: '',
        hayAnunciosPropios: false,
        hayAnunciosConmigo: false,
        showAnuncios: false
    },
    mounted() {
        this.getConversations()
    },
    methods: {

        showConversations: function (type) {

            if (type) {
                this.showAnuncios = false
            } else {
                this.showAnuncios = true
            }

        },

        // Obtenemos todas las conversaciones de la BD
        getConversations: function () {

            if (localStorage.getItem("token") != null) {

                var config = {
                    headers: { 'Authorization': "bearer " + localStorage.getItem("token") }
                };

                // CONVERSACIONES PROPIAS
                axios.get(`http://${ip}:${puerto}/conversacionespropias`,
                    config

                ).then(function (res) {

                    if (res.data.conversaciones != 0 && res.data.conversaciones != null) {

                        let arrayConversaciones = []

                        res.data.conversaciones.forEach(conversacion => {
                            conversacion.usuario.imagen = `http://${ip_nginx}:80/usuarios/${conversacion.usuario.imagen}`

                            arrayConversaciones.push(conversacion);
                        })

                        app.chatsPropios = arrayConversaciones
                        app.hayAnunciosPropios = true

                        app.own_id = res.data.id_usuario

                    }

                }).catch(function (error) {

                    $.growl.warning({ location: 'tr', message: "No se han podido obtener las conversaciones" });

                })


                // CONVERSACIONES CONMIGO
                axios.get(`http://${ip}:${puerto}/conversacionesmisanuncios`,
                    config

                ).then(function (res) {


                    let arrayConversaciones = []

                    if (res.data.conversaciones != 0 && res.data.conversaciones != null) {

                        res.data.conversaciones.forEach(conversacion => {

                            conversacion.usuario.imagen = `http://${ip_nginx}:80/usuarios/${conversacion.usuario.imagen}`

                            arrayConversaciones.push(conversacion);

                        })

                        app.chatsConmigo = arrayConversaciones
                        app.hayAnunciosConmigo = true

                        app.own_id = res.data.id_usuario

                        console.log(app.chatsConmigo)
                    }

                }).catch(function (error) {

                    $.growl.warning({ location: 'tr', message: "No se han podido obtener las conversaciones" });

                })

            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }


        },

        // Si le dan a chatear redirimos a la pantalla del chat
        startChat: function (id_anuncio, user, type) {

            if (localStorage.getItem("token") != null) {

                if (type == "propia") {

                    localStorage.setItem("user_chat", JSON.stringify({ nombre: user.nombre, id: user.id, own_id: app.own_id, firsttime: false, id_firebase: app.own_id, id_anuncio: id_anuncio }));

                } else {

                    localStorage.setItem("user_chat", JSON.stringify({ nombre: user.nombre, id: user.id, own_id: app.own_id, firsttime: false, id_firebase: user.id, id_anuncio: id_anuncio }));

                }

                window.location = "chat.html"

            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }


        }

    }

})