
var app = new Vue({
    el: '.chat_window',
    data: {
        message: '',
        aMensajes: [],
        db: '',
        userChat: '',
        ownId: '',
        id_anuncio: '',
        state: true,
        firebase_id: ''
    },
    mounted() {
        this.startChat()
        this.getAllMessages()
    },
    methods: {

        // Obtenemos datos del localStorage para saber quien habla con quien
        startChat: function () {

            this.db = firebase.firestore();

            this.userChat = JSON.parse(localStorage.getItem("user_chat"));
            this.ownId = this.userChat.own_id
            this.id_anuncio = this.userChat.id_anuncio
            this.firebase_id = this.userChat.id_firebase

            if (this.ownId == null || this.ownId == undefined || this.id_anuncio == null || this.ownId == undefined || this.userChat == null || this.userChat == undefined) {
                $.growl.warning({ location: 'tr', message: "Algo ha ido mal, vuelve a intentarlo" });
                this.state = false
            }

        },

        // Obtenemos todos los mensajes previos de firebase
        getAllMessages: function () {

            if (this.state) {

                this.db.collection(`${this.firebase_id}-${this.id_anuncio}`).onSnapshot(function (query) {

                    let arrayMensajes = []

                    query.forEach(function (doc) {

                        let data = doc.data()

                        if (data.user == app.ownId) {
                            arrayMensajes.unshift({ text: data.message, side: true, fecha: data.fecha })

                        } else if (data.user == app.userChat.id) {
                            arrayMensajes.unshift({ text: data.message, side: false, fecha: data.fecha })
                        }


                    });

                    app.aMensajes = arrayMensajes.sort(app.compare)

                })

            }

        },

        // Comparamos la fecha de cada mensaje para saber cuál va primero
        compare: function (a, b) {

            if (a.fecha > b.fecha) {
                return 1;
            }

            if (a.fecha < b.fecha) {
                return -1;
            }

            return 0;
        },

        // Al enviar un mensaje validamos el contenido y lo guardamos en firebase
        sendMessage: function () {

            if (this.state) {

                let mensaje = this.message

                if (mensaje.trim() === '') {
                    return;
                }

                let fecha = new Date();

                this.db.collection(`${this.firebase_id}-${this.id_anuncio}`).add({
                    user: this.ownId,
                    message: mensaje,
                    fecha: fecha.getTime()
                })

                this.message = ""

            }

        },

    }

})


