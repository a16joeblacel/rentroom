var ownAnuncios = new Vue({
    el: '#mis_anunciosBox',
    data: {
        anuncios: "",
        update: {
            precio: '',
            direccion: '',
            provincia: '',
            ciudad: '',
            lavabo: '',
            tam_casa: '',
            tam_hab: '',
            num_hab: '',
            tam_camas: '',
            num_cam: '',
            ventana: ''
        },
        selectedAnuncio: ''
    },
    mounted() {
        this.getMisAnuncios()
        this.setModal()
    },
    methods: {

        // Si hacen click en el anuncio redirigimos a él
        showAnuncio: function (id) {
            window.location.href = `anuncio.html?id=${id}`
        },

        // Obtenemos todos los anuncios propios al cargar la página
        getMisAnuncios: function () {

            if (localStorage.getItem("token") != null) {

                var config = {
                    headers: { 'Authorization': "bearer " + localStorage.getItem("token") }
                };

                axios.get(`http://${ip}:${puerto}/myanuncios`,
                    config

                ).then(function (res) {

                    let arrayAnuncios = [];

                    res.data.forEach(anuncio => {
                        anuncio.imagen = `http://${ip_nginx}:80/anuncios/${anuncio.imagen}`
                        anuncio.fecha_publicacion = anuncio.fecha_publicacion.split("T")[0]

                        let fecha = anuncio.fecha_publicacion.split("-")

                        anuncio.fecha_publicacion = `${fecha[2]} - ${fecha[1]} - ${fecha[0]}`

                        arrayAnuncios.push(anuncio);
                    })

                    ownAnuncios.anuncios = arrayAnuncios

                }).catch(error => {

                    $.growl.error({ location: 'tr', message: "No se han podido obtener los anuncios" });

                })

            }

        },

        // Configuramos el modal y los botones
        setModal: function () {

            $(document).ready(() => {

                $(".second").hide()

                $(".first-tab").click(() => {
                    $(".first").show()
                    $(".second").hide()

                })

                $(".second-tab").click(() => {
                    $(".first").hide()
                    $(".second").show()

                    $('#select_element').find('option[selected="selected"]').each(function () {
                        $(this).prop('selected', true);
                    });
                })

            })

        },

        // Al darle a editar, hacemos que aparezca el modal
        edit_anuncio: function (anuncio) {

            this.selectedAnuncio = anuncio

            $('#exampleModal').modal('show')
        },

        // Eliminamos el anuncio seleccionado y todo lo asociado a él en el servidor
        deleteAnuncio: function () {

            if (localStorage.getItem("token") != null) {

                const request = axios.create({
                    headers: {
                        Authorization: "bearer " + localStorage.getItem("token")
                    }
                });

                request.delete(`http://${ip}:${puerto}/anuncio`,
                    {
                        data: {
                            id: this.selectedAnuncio.id
                        }

                    }).then(function (res) {

                        if (res.data) {
                            location.reload()
                        } else {
                            $.growl.error({ location: 'tr', message: "No se ha podido eliminar el anuncio" });
                        }


                    }).catch(function (error) {

                        $.growl.error({ location: 'tr', message: "No se ha podido eliminar el anuncio" });

                    })


            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }


        },

        // Actualizamos los datos del anuncio tras validar los campos
        updateAnuncio: function () {

            if (localStorage.getItem("token") != null) {

                let paramsArray = [this.update.precio, this.update.provincia, this.update.ciudad, this.update.direccion, this.update.lavabo, this.update.tam_casa, this.update.tam_hab, this.update.num_hab, this.update.tam_cama, this.update.num_camas, this.update.ventana, this.update.descripcion]

                let resultValidacion = this.validarCampos(paramsArray)

                if (resultValidacion.status) {

                    var config = {
                        headers: {
                            'Authorization': "bearer " + localStorage.getItem("token")
                        }
                    };

                    axios.put(`http://${ip}:${puerto}/anuncio`,
                        { update: this.update, id: this.selectedAnuncio.id },
                        config

                    ).then(function (res) {

                        if (res.data.updated) {

                            location.reload()

                        } else {

                            $.growl.error({ location: 'tr', message: "No se ha podido actualizar el anuncio" });

                        }

                    }).catch(function (error) {

                        $.growl.error({ location: 'tr', message: "No se ha podido actualizar el anuncio" });

                    })

                } else {

                    $.growl.error({ location: 'tr', message: resultValidacion.message });

                }

            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }

        },

        // Valida todos los campos recibidos como array, devuelve true o false y el mensaje de error
        validarCampos: function (campos) {

            let status = false
            let message = ""

            // Comprovamos que no haya ningun campo vacío
            for (x = 0; x < campos.length; x++) {

                if (campos[x] === "" || campos[x] === undefined) {
                    status = false
                    message = "No pueden haber campos vacíos"

                    return { 'status': status, 'message': message }
                }

            }

            // Validaciones especiales de cantidad o de carácteres
            if (!(campos[0] > 50 && campos[0] < 10000)) {

                message = "El precio es demasiado bajo o demasiado alto"

            } else if (!/^[a-zA-Z]+$/.test(campos[2])) {

                message = "La ciudad no puede contener números"

            } else if (campos[5] < 10 || campos[5] > 10000) {

                message = "El tamaño de la casa es demasiado bajo o demasiado alto"

            } else if (campos[6] < 3 || campos[6] > 1000) {

                message = "El tamaño de la habitación es demasiado bajo o demasiado alto"

            } else if (campos[7] < 1 || campos[7] > 100) {

                message = "El número de habitaciones es demasiado bajo o demasiado alto"

            } else if (campos[9] < 1 || campos[9] > 10) {

                message = "El número de camas es demasiado bajo o demasiado alto"

            } else if (campos[11].length < 10 || campos[11] > 500) {

                message = "La descripción es demasiado corta o demasiado larga"

            } else {

                status = true

            }

            return { 'status': status, 'message': message }

        },

        // Cambiamos el estado del anuncio, para que ya no sea visible en la pantalla principal
        alquilado: function (id) {

            if (localStorage.getItem("token") != null) {

                var config = {
                    headers: {
                        'Authorization': "bearer " + localStorage.getItem("token")
                    }
                };

                axios.put(`http://${ip}:${puerto}/anuncioalquilado`,
                    { id: id },
                    config

                ).then(function (res) {

                    window.location.reload()

                }).catch(function (error) {

                    $.growl.error({ location: 'tr', message: "No se ha podido dar como alquilada la habitación" });

                })

            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }

        }

    }

})


