$(document).ready(function () {

  // Hacemos que la barra superior sea más pequeña al hacer scroll hacia abajo y sea fija
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.site-navbar h1').addClass('shrink');
      $('.barra_createpub').hide();
      $('.site-wrap').css('position', 'fixed')

    } else {
      $('.site-navbar h1').removeClass('shrink');
      $('.barra_createpub').show();
      $('.site-wrap').css('position', 'relative')

    }
  });

});

