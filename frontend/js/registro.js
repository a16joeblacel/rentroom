var app = new Vue({
    el: '#div_inicial',
    data: {
        nombre: "",
        email: "",
        contraseña: "",
        contraseña2: "",
    },
    methods: {

        // Comprovamos los datos introducidos y los guardamos en la BD, tras ello les logueamos automáticamente
        register_action: function () {

            if (this.contraseña == this.contraseña2) {

                if (this.validarCampo(this.nombre, "text")) {

                    if (this.validarCampo(this.email, "email")) {

                        if (this.validarCampo(this.contraseña, "password")) {

                            axios.post(`http://${ip}:${puerto}/register`,
                                {
                                    nombre: app.nombre,
                                    email: app.email,
                                    password: app.contraseña

                                }).then(function (res) {

                                    console.log(res)

                                    if (res.status == 200) {

                                        localStorage.setItem("token", res.data.token);
                                        localStorage.setItem("nombre", app.nombre);

                                        window.location.replace("index.html")

                                    } else {

                                        $.growl.warning({ location: 'tr', message: "No se ha podido realizar el registro" });

                                    }

                                }).catch(error => {

                                    $.growl.error({ location: 'tr', message: "No se ha podido realizar el registro" });

                                })

                        } else {

                            $.growl.warning({ location: 'br', message: "La contraseña no cumple con los requisitos" });

                        }

                    } else {

                        $.growl.warning({ location: 'br', message: "El email no cumple con los requisitos" });

                    }

                } else {

                    $.growl.warning({ location: 'br', message: "El nombre no cumple con los requisitos" });

                }

            } else {

                $.growl.warning({ location: 'br', message: "Las contraseñas no coinciden" });

                // Mostrar mensaje contraseñas diferentes al usuario
            }
        },

        // Validación de los campos (email y password)
        validarCampo: function (text, type) {

            if (type == 'text') {
                if (text === "" || !/^[a-zA-Z]+$/.test(text) || text.length < 3) {

                    return false
                }
            }

            if (type == 'email') {

                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (text === "" || text.length < 5 || !re.test(String(text).toLowerCase())) {
                    return false
                }

            }

            if (type == 'password') {

                if (text === "" || text.length < 8 || !/\d/.test(text) || !text.match(/[A-Z]/)) {
                    return false
                }

            }

            return true;

        }

    }

})