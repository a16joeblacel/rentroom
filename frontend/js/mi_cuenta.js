
var app = new Vue({
    el: '#miCuenta',
    data: {
        perfil: {
            nombre: '',
            edad: '',
            imagen: '',
            genero: '',
            idiomas: [],
        },
        imgStatus: 'empty',
        imgShow: false
    },
    mounted() {

    },
    methods: {

        // Comprovamos que la imagen tenga el peso adecuado y guardamos una referencia de ella
        uploadImage: function () {

            app.imgShow = false
            app.imgStatus = 'failed'

            let image = this.$refs.miImagen.files[0];

            if (image.size > 10 * 1024 * 1024) {

                $.growl.warning({ title: 'la imagen pesa demasiado', location: 'tr', message: "Máximo peso imagen 1 MB" });

            } else {

                app.perfil.image = image;
                app.imgStatus = 'correct'

                $(".custom_filename").html(app.perfil.image.name)

            }
        },

        // Lanzamos una petición al servidor después de validar los campos y actualizamos los datos
        updateProfile: function () {

            if (localStorage.getItem("token") != null) {

                let camposArray = [this.perfil.nombre, this.perfil.edad, this.perfil.genero]

                let validacion = this.validarCampos(camposArray)

                if (validacion.status) {

                    if (app.imgStatus == 'correct' || app.imgStatus == 'empty') {

                        var config = {
                            headers: {
                                'Content-Type': 'multipart/form-data',
                                'Authorization': "bearer " + localStorage.getItem("token")
                            }
                        };

                        let paramsArray = [this.perfil.nombre, this.perfil.edad, this.perfil.genero, this.perfil.idiomas]

                        let formData = new FormData();
                        formData.append('file', this.perfil.image);
                        formData.append('perfil', paramsArray);

                        axios.put(`http://${ip}:${puerto}/user`,
                            formData,
                            config

                        ).then(function (res) {

                            window.location.reload()

                        }).catch(function (error) {

                            $.growl.error({ location: 'tr', message: "No se ha podido actualizar el perfil" });

                        })

                    } else {

                        if (app.imgStatus == "wrong") {

                            $.growl.error({ location: 'tr', message: "La imagen pesa demasiado, selecciona otra" });

                        } else if (app.imgStatus == "empty") {

                            $.growl.error({ location: 'tr', message: "Debes seleccionar una imagen" });

                        }

                    }

                } else {

                    $.growl.error({ location: 'br', message: validacion.message });

                }

            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }

        },

        // Validación de campos para que no esten vacios
        validarCampos: function (campos) {

            let status = false
            let message = ""

            for (x = 0; x < campos.length; x++) {

                if (campos[x] === "" || campos[x] === undefined) {
                    status = false
                    message = "No pueden haber campos vacíos"

                    return { 'status': status, 'message': message }
                }

            }

            return { 'status': true, 'message': message }

        },

        // Obtenemos los datos antiguos del usuario de la BD y los mostramos
        cargaDatosUsuario: function () {

            if (localStorage.getItem("token") != null) {

                var config = {
                    headers: {
                        'Authorization': "bearer " + localStorage.getItem("token")
                    }
                };

                axios.get(`http://${ip}:${puerto}/user`,
                    config

                ).then(function (res) {

                    app.perfil = res.data
                    app.perfil

                    res.data.idiomas.forEach(idioma => {
                        app.perfil.idiomas.push(idioma.nombre)
                    })

                    app.perfil.imagen = `http://${ip_nginx}:80/usuarios/${app.perfil.imagen}`

                    app.imgShow = true

                }).catch(function (error) {

                    $.growl.error({ location: 'tr', message: "No se ha podido obtener la información del perfil" });

                })

            }

        }


    }

})