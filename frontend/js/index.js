
var app = new Vue({
    el: '#inicial',
    data: {
        allAnuncios: [],
        anunciosVisibles: [],
        anuncios: [],
        button: 1
    },

    mounted() {
        this.getAllAnuncios()
    },

    methods: {

        // Obtenemos un array con todas las ciudades, solo mostramos 6 y el resto las guardamos en otro array
        getAllAnuncios: function () {

            axios.get(`http://${ip}:${puerto}/anuncios`).then(function (res) {

                let arrayAnuncios = [];
                app.allAnuncios = []

                let counter = 0;

                res.data.forEach(anuncio => {

                    anuncio.imagen = `http://${ip_nginx}:80/anuncios/${anuncio.imagen}`
                    app.allAnuncios.push(anuncio)
                    app.anuncios.push(anuncio)

                    counter++
                    if (counter <= 6) {
                        arrayAnuncios.push(anuncio);
                    }

                })

                app.anunciosVisibles = arrayAnuncios

            }).catch(error => {

                $.growl.error({ location: 'tr', message: "No se han podido obtener los anuncios!" });

            })

        },

        // Cuando hacen click en un botón para ver más anuncios, cambiamos el array de anunciosvisibles
        nextAnuncios: function (num) {

            if (!$(`.site-pagination div[but=${num}]`).hasClass("active")) {

                $(`.site-pagination div[but=${this.button}]`).removeClass("active")

                $(`.site-pagination div[but=${num}]`).addClass("active")

                this.button = num

                let arrayAnuncios = [];

                let counter = 0

                if (num == 1) {

                    app.allAnuncios.forEach(anuncio => {

                        counter++
                        if (counter <= 6) {
                            arrayAnuncios.push(anuncio);
                        }
                    })

                } else {

                    app.allAnuncios.forEach(anuncio => {
                        counter++
                        if ((counter >= (num - 1) * 6) && (counter < num * 6)) {

                            arrayAnuncios.push(anuncio);
                        }
                    })

                }

                app.anunciosVisibles = arrayAnuncios

            }

        },


    }
})

var filtro = new Vue({
    el: '#filter_box',
    data: {
        key: {
            tam_hab: 'Tamaño Habitación',
            tam_casa: 'Tamaño Casa',
            provincia: 'Provincia',
            lavabo: 'Lavabo'
        },
        min: 250,
        max: 650
    },

    mounted() {
        this.setSlider()
    },

    methods: {

        // Configuramos el filtro de precio
        setSlider: function () {

            $("#slider-range").slider({
                range: true,
                min: 50,
                max: 1500,
                values: [250, 650],
                slide: function (event, ui) {
                    $("#amount").val(ui.values[0] + "€ - " + ui.values[1] + " €");
                    filtro.min = ui.values[0];
                    filtro.max = ui.values[1];
                }

            });

            $("#amount").val($("#slider-range").slider("values", 0) +
                "€ - " + $("#slider-range").slider("values", 1) + " €");

        },

        // Delimitamos el array de anuncios en función del filtro escogido
        filtrar: function () {

            let newArray = []
            let arrayVisibles = []

            app.anuncios.forEach(anuncio => {

                if (anuncio.lavabo == false) {

                    anuncio.lavabo = "Individual";
                }

                if (anuncio.lavabo == true) {

                    anuncio.lavabo = "Compartido";
                }

                if (anuncio.precio > this.min && anuncio.precio < this.max) {

                    if (anuncio.tamano_hab >= this.key.tam_hab || this.key.tam_hab == 'Tamaño Habitación') {

                        if (anuncio.tamano_casa >= this.key.tam_casa || this.key.tam_casa == 'Tamaño Casa') {

                            if (anuncio.lavabo == this.key.lavabo || this.key.lavabo == 'Lavabo') {

                                if (anuncio.provincia == this.key.provincia || this.key.provincia == 'Provincia') {

                                    newArray.push(anuncio);

                                    if (arrayVisibles.length < 6) {

                                        arrayVisibles.push(anuncio)

                                    }

                                }

                            }

                        }

                    }

                }

            });

            app.allAnuncios = newArray
            app.anunciosVisibles = arrayVisibles

        },

        resetFiltro: (() => {

            app.allAnuncios = app.anuncios
            app.anunciosVisibles = []

            for (let x = 0; x < 6; x++) {
                app.anunciosVisibles.push(app.allAnuncios[x])
            }

        })
    }

})

var count = new Vue({
    el: '#count_ciudades',
    data: {
        count_bcn: 0,
        count_val: 0,
        count_mad: 0,
        count_bal: 0,
        count_tar: 0,
        count_zar: 0,
        count_sev: 0

    },

    mounted() {
        this.setEffect()
        this.getAllCiudades()
    },

    methods: {

        setEffect: function () {

            AOS.init({
                duration: 300,
                easing: 'slide',
                once: true
            });

        },

        // Obtenemos el contador de anuncios para cada una de las ciudades
        getAllCiudades: function () {

            axios.get(`http://${ip}:${puerto}/ciudades`).then(function (res) {

                count.count_bcn = res.data.barcelona
                count.count_val = res.data.valencia
                count.count_mad = res.data.madrid
                count.count_bal = res.data.baleares
                count.count_tar = res.data.tarragona
                count.count_zar = res.data.zaragoza
                count.count_sev = res.data.sevilla

            }).catch(error => {

                $.growl.error({ location: 'tr', message: "No se han podido obtener la cantidad de habitaciones por ciudad!" });

            })


        }
    }

});
