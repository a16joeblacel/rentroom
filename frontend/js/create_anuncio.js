var createAnuncio = new Vue({
    el: '#div_inicial',
    data: {
        anuncio: {

        },
        image: '',
        imageStatus: 'empty',
        servicios: [
            { nombre: 'Wifi', imagen: './images/servicios/wifi.png', checked: false },
            { nombre: 'Terraza', imagen: './images/servicios/terraza.png', checked: false },
            { nombre: 'Portero', imagen: './images/servicios/portero.png', checked: false },
            { nombre: 'Secadora', imagen: './images/servicios/secadora.png', checked: false },
            { nombre: 'Balcón', imagen: './images/servicios/balcon.png', checked: false },
            { nombre: 'Aire acondicionado', imagen: './images/servicios/aire-acondicionado.png', checked: false },
            { nombre: 'Calefacción', imagen: './images/servicios/calefaccion.png', checked: false },
            { nombre: 'Lavadora', imagen: './images/servicios/lavadora.png', checked: false },
            { nombre: 'Ascensor', imagen: './images/servicios/ascensor.png', checked: false },
            { nombre: 'Lavavajillas', imagen: './images/servicios/lavavajillas.png', checked: false },
            { nombre: 'Accessible', imagen: './images/servicios/accessible.png', checked: false },
            { nombre: 'Aparcamiento', imagen: './images/servicios/aparcamiento.png', checked: false },

        ],
    },
    mounted() {

        // Obtenemos la galeria de imagenes
        $('#filesToUpload').change(function () {

            var input = document.getElementById('filesToUpload');

            $(".custom_multi_filename").html(input.files.length + " fitxers")

        });

        // Si no está logueado no permitimos crear un anuncio y le redirigimos al registro
        if (localStorage.getItem("token") == null) {

            window.location = "registro.html"

        }

    },

    methods: {

        // Checkbuttons de los servicios
        cambiarEstado: function (servicio) {

            if (servicio.checked)
                servicio.checked = false
            else
                servicio.checked = true
        },

        // Comprovamos que la imagen sea la correcta cuando la escoge y guardamos una referencia de ella
        uploadImage: function () {

            var input = document.getElementById('filesToUpload');

            let image = this.$refs.miImagen.files[0];

            if (image) {

                createAnuncio.imageStatus = "wrong"

                if (image.size > 10 * 1024 * 1024) {

                    $.growl.warning({ title: 'La imagen principal pesa demasiado', location: 'tr', message: "Máximo peso imagen 10 MB" });

                } else {

                    createAnuncio.image = image;

                    createAnuncio.imageStatus = "correct"

                    $(".custom_filename").html(createAnuncio.image.name)

                }

            }

        },

        // Lee los datos introducidos y envia una petición ajax al servidor para guardarlos en la BD
        createAnuncio: function () {

            if (localStorage.getItem("token") != null) {

                let checkedServices = []

                // Comprovamos que servicios ha marcado
                this.servicios.forEach(serv => {
                    if (serv.checked)
                        checkedServices.push(serv.nombre)
                })

                let paramsArray = [this.anuncio.precio, this.anuncio.provincia, this.anuncio.ciudad, this.anuncio.direccion, this.anuncio.lavabo, this.anuncio.tam_casa, this.anuncio.tam_hab, this.anuncio.num_hab, this.anuncio.tam_cama, this.anuncio.num_camas, this.anuncio.ventana, this.anuncio.descripcion]

                let resultValidacion = this.validarCampos(paramsArray)

                // Comprovamos que ha superado las validaciones
                if (resultValidacion.status) {

                    if (createAnuncio.imageStatus == "correct") {

                        var input = document.getElementById('filesToUpload');

                        let validacionGaleria = this.validarGaleriaImg(input.files)

                        if (validacionGaleria.status) {

                            var config = {
                                headers: {
                                    'Content-Type': 'multipart/form-data',
                                    'Authorization': "bearer " + localStorage.getItem("token")
                                }
                            };

                            let formData = new FormData();
                            formData.append('file', this.image);
                            formData.append('anuncio', paramsArray);
                            formData.append('servicios', checkedServices)

                            // Lanzamos petición al servidor para guardar datos del anuncio
                            axios.post(`http://${ip}:${puerto}/anuncio`,
                                formData,
                                config

                            ).then(function (res) {

                                if (res.data.created) {

                                    for (let j = 0; j < input.files.length; j++) {

                                        let formImages = new FormData();
                                        formImages.append('file', input.files[j]);
                                        formImages.append('anuncio', res.data.anuncio);

                                        axios.post(`http://${ip}:${puerto}/imagengaleria`,
                                            formImages,
                                            config

                                        ).then(function (result2) {

                                            if (j == (input.files.length - 1)) {

                                                window.location.href = `anuncio.html?id=${res.data.anuncio}`

                                            }

                                        })

                                    }


                                } else {

                                    $.growl.error({ location: 'tr', message: "No se ha podido crear el anuncio" });

                                }


                            }).catch(function (error) {

                                $.growl.error({ location: 'tr', message: "No se ha podido crear el anuncio" });

                            })


                        } else {

                            $.growl.error({ location: 'tr', message: validacionGaleria.message });

                        }

                    } else {

                        if (createAnuncio.imageStatus == "wrong") {

                            $.growl.error({ location: 'tr', message: "La imagen principal pesa demasiado, selecciona otra" });

                        } else if (createAnuncio.imageStatus == "empty") {

                            $.growl.error({ location: 'tr', message: "Debes seleccionar una imagen principal" });

                        }

                    }


                } else {

                    $.growl.error({ location: 'tr', message: resultValidacion.message });

                }

            } else {

                $.growl.error({ location: 'tr', message: "No estás logueado" });

            }

        },

        // Valida las imagenes de la galeria, devuelve true o false y la imagen érronea
        validarGaleriaImg: function (files) {

            let status = false
            let message = ""

            if (files.length == 0) {

                message = "Debes seleccionar mínimo una imagen para la galería"

                return { 'status': status, 'message': message }


            }

            if (files.length > 8) {

                message = "No se pueden seleccionar más de 8 imagenes para la galería"

                return { 'status': status, 'message': message }


            }

            for (let j = 0; j < files.length; j++) {

                if (files[j].size > 10 * 1024 * 1024) {

                    let aux = j

                    aux++

                    message = "La imagen " + aux + " pesa más de 10 MB, selecciona otra"

                    return { 'status': status, 'message': message }

                }

            }

            return { 'status': true, 'message': message }

        },

        // Valida todos los campos recibidos como array, devuelve true o false y el mensaje de error
        validarCampos: function (campos) {

            let status = false
            let message = ""

            // Comprovamos que no haya ningun campo vacío
            for (x = 0; x < campos.length; x++) {

                if (campos[x] === "" || campos[x] === undefined) {
                    status = false
                    message = "No pueden haber campos vacíos"

                    return { 'status': status, 'message': message }
                }

            }

            // Validaciones especiales de cantidad o de carácteres
            if (!(campos[0] > 50 && campos[0] < 10000)) {

                message = "El precio es demasiado bajo o demasiado alto"

            } else if (!/^[a-zA-Z]+$/.test(campos[2])) {

                message = "La ciudad no puede contener números"

            } else if (campos[5] < 10 || campos[5] > 10000) {

                message = "El tamaño de la casa es demasiado bajo o demasiado alto"

            } else if (campos[6] < 3 || campos[6] > 1000) {

                message = "El tamaño de la habitación es demasiado bajo o demasiado alto"

            } else if (campos[7] < 1 || campos[7] > 100) {

                message = "El número de habitaciones es demasiado bajo o demasiado alto"

            } else if (campos[9] < 1 || campos[9] > 10) {

                message = "El número de camas es demasiado bajo o demasiado alto"

            } else if (campos[11].length < 10 || campos[11] > 500) {

                message = "La descripción es demasiado corta o demasiado larga"

            } else {

                status = true

            }

            return { 'status': status, 'message': message }

        }
    }
})